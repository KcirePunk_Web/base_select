import React, { useEffect, useState } from 'react';
import { Select, SelectEvent } from '../@material/forms/Select';
import { getFormatoPlacaDataList } from '../services/config.service';
import { getClaseVehiculoDataList } from '../services/vehiculo.service';

export const Config2 = () => {
	const [data, setData] = useState<any>([]);
	const [data2, setData2] = useState<any>([]);

	const handleChange = (event: SelectEvent) => {};

	useEffect(() => {
		getFormatoPlacaDataList().then((resp: any[]) => {
			setData2([...resp]);
		});
		getClaseVehiculoDataList().then((resp: any[]) => {
			console.log(resp);

			setData([...resp]);
			// console.log(resp);
		});
	}, []);

	return (
		<div className="container p-5">
			<Select
				data={data}
				id="config2"
				label="Config2"
				onChange={handleChange}
			/>

			<Select
				data={data2}
				id="config2"
				label="Data 2"
				onChange={handleChange}
			/>
		</div>
	);
};
