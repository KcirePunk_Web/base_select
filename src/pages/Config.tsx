import React, { useEffect, useState } from 'react';
import { Select, SelectEvent } from '../@material/forms/Select';
import { getFormatoPlacaDataList } from '../services/config.service';

export const Config = () => {
	const [data2, setData2] = useState<any>([]);

	const handleChange = (event: SelectEvent) => {};

	useEffect(() => {
		getFormatoPlacaDataList().then((resp: any[]) => {
			setData2([...resp]);
		});
	}, []);

	return (
		<div className="container p-5">
			<Select
				data={data2}
				id="config2"
				label="Data 2"
				onChange={handleChange}
			/>
		</div>
	);
};
