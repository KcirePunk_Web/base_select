import axios from 'axios'
import * as utils from '../utils/object.mappings';

const URL = 'https://sigett.herokuapp.com/sigett.apiservice/entidadconfig/listar';

export const getFormatoPlaca = () => {
    return axios.get('https://sigett.herokuapp.com/sigett.apiservice/entidadconfig/listar?entidad=FORMATO_PLACA');
}

export const getFormatoPlacaDataList = () => {
    return axios.get('https://sigett.herokuapp.com/sigett.apiservice/entidadconfig/listar?entidad=FORMATO_PLACA').then(resp => {
        return utils.objectMapList(resp.data.data, 'id_unico', ['detalle']);
    })
}


export const getAccionArchivo = () => {
    return axios.get('https://sigett.herokuapp.com/sigett.apiservice/entidadconfig/listar?entidad=ACCION_ARCHIVO');
}

export const getEntidadConfig = (): Promise<string[]> => {
    return axios.get(URL).then(resp => {
        return Object.keys(resp.data.data)
    })
}

export const getConfig = (entidad: string) => {
    return axios.get(`${URL}?entidad=${entidad}`).then(resp => resp.data)
}