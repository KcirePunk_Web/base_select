import axios from 'axios'
import * as utils from '../utils/object.mappings';

export const getClaseVehiculo = () => {
    return axios.get('https://sigett.herokuapp.com/sigett.apiservice/vehiculos/clase/listar').then(resp => resp.data);
}

export const getClaseVehiculoDataList = () => {
    return axios.get('https://sigett.herokuapp.com/sigett.apiservice/vehiculos/clase/listar').then(resp => {
        return utils.objectMapList(resp.data.data, 'cod_claveh', ['detalle', 'ref_tarifario']);
    });
}