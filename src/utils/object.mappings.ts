export const objectMapList = (arr: any[], value_column: string, text_columns: Array<string>) => {
    return arr.map(item => {
        const objRetorno = text_columns.map((text_column: string) => {  
            let newValor = item[text_column] + ' ';

            return newValor
        })
        return {
            value: item[value_column],
            text: objRetorno.join('')
        }
    })
}
