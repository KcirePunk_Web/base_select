<?php
function arrayToComboList($arrayDatos, $poscolvalue, $listacolstext){
        $resultArray = NULL;
        
        if (count($arrayDatos) > 0) 
        {
            for ($i=0; $i<count($arrayDatos); $i++)
            {
                $temp = $arrayDatos[$i];
                $valor = "";
                if (strpos($listacolstext,",") !== false){
                    $a = explode(",",$listacolstext);
                    for ($j=0; $j<count($a); $j++){
                        $valor .= $this->forceUTF8($temp[intval($a[$j])]) . " ";
                    }
                }else {
                    $valor = $this->forceUTF8($temp[intval($listacolstext)]);
                }
                $resultArray[$i] = array("value" => $temp[$poscolvalue], "text" => $valor);
            }
        }
        return $resultArray;
    }

    