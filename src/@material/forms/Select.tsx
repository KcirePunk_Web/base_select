import React from 'react';

export type SelectEvent = React.ChangeEvent<HTMLSelectElement>;

export type SelectProps = {
	id: string;
	value?: string;
	onChange(event: SelectEvent): void;
	disabled?: boolean;
	label?: string;
	data: any[];
};

export const Select: React.FC<SelectProps> = ({
	id,
	value = '',
	onChange,
	disabled = false,
	label = '',
	data,
}) => {
	const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		onChange(e);
	};

	return (
		<div className="form-group">
			{label !== '' && <label htmlFor={id}>{label}</label>}
			<select
				id={id}
				disabled={disabled}
				value={value}
				onChange={handleChange}
				className="form-select"
				aria-label="Default select example">
				<option value="" disabled>
					Seleccionar
				</option>
				{data.map((item: { value: string; text: string }) => (
					<option key={item.value} value={item.value}>
						{item.text}
					</option>
				))}
			</select>
		</div>
	);
};
